//
//  BBAPIService.swift
//  BBApp
//
//  Created by James Bellamy on 29/05/2021.
//

import Combine
import Foundation

protocol BreakingBadCharactersServiceProtocol: OffsetPaginationServiceProtocol {
    func getAllCharacters(category: String?, name: String?) -> AnyPublisher<[BreakingBadCharacter], APIError>
}

final class BreakingBadCharactersService: OffsetPaginationService, BreakingBadCharactersServiceProtocol {
    // Requests
    private var cancellables: [AnyCancellable] = []
    
    // MARK: Actions
    
    func getAllCharacters(category: String?, name: String?) -> AnyPublisher<[BreakingBadCharacter], APIError> {
        guard self.canFetch == true else {
            let error = APIError.request(description: "Pagination Limit Reached")
            return Fail(error: error).eraseToAnyPublisher()
        }
        let client: APIClientProtocol = APIClient(session: URLSession.shared)
        let request: APIRequest = BreakingBadEndpoint.getCharacters(
            limit: self.limit,
            offset: self.offset,
            category: category,
            name: name
        )
        let publisher: AnyPublisher<[BreakingBadCharacter], APIError> = client.perform(request: request)
        publisher.sink(
            receiveCompletion: { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .failure: self.canFetch = false
                case .finished: break
                }
            },
            receiveValue: { [weak self] (characters) in
                guard let self = self else { return }
                self.next(after: characters.count)
            }
        ).store(in: &self.cancellables)
        return publisher
    }
}
