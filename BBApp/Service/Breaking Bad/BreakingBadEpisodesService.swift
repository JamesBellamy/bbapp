//
//  BreakingBadEpisodesService.swift
//  BBApp
//
//  Created by James Bellamy on 30/05/2021.
//

import Combine
import Foundation

protocol BreakingBadEpisodeServiceProtocol {
    func getAllEpisodes(series: String?) -> AnyPublisher<[BreakingBadEpisode], APIError>
}

final class BreakingBadEpisodesService: BreakingBadEpisodeServiceProtocol {
    func getAllEpisodes(series: String?) -> AnyPublisher<[BreakingBadEpisode], APIError> {
        let client: APIClientProtocol = APIClient(session: URLSession.shared)
        let request: APIRequest = BreakingBadEndpoint.getEpisodes(series: series)
        return client.perform(request: request)
    }
}
