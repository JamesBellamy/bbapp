//
//  BreakingBadQuotesService.swift
//  BBApp
//
//  Created by James Bellamy on 30/05/2021.
//

import Combine
import Foundation

protocol BreakingBadQuotesServiceProtocol {
    func getAllQuotes() -> AnyPublisher<[BreakingBadQuote], APIError>
}

final class BreakingBadQuotesService: BreakingBadQuotesServiceProtocol {
    func getAllQuotes() -> AnyPublisher<[BreakingBadQuote], APIError> {
        let client: APIClientProtocol = APIClient(session: URLSession.shared)
        let request: APIRequest = BreakingBadEndpoint.getQuotes
        return client.perform(request: request)
    }
}
