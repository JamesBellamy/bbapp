//
//  OffsetPaginationService.swift
//  BBApp
//
//  Created by James Bellamy on 29/05/2021.
//

import Foundation

protocol OffsetPaginationServiceProtocol {
    var limit: Int { get }
    var offset: Int { get }
    var canFetch: Bool { get }
    
    func reset()
    func next(after count: Int)
}

class OffsetPaginationService: OffsetPaginationServiceProtocol {
    
    let limit: Int
    var offset: Int = 0
    var canFetch: Bool = true
    
    init(limit: Int) {
        self.limit = limit
    }
    
    // MARK: Actions
    
    func reset() {
        self.offset = 0
        self.canFetch = true
    }
    
    func next(after count: Int) {
        if count < limit {
            self.canFetch = false
        } else {
            self.offset += self.limit
        }
    }
}
