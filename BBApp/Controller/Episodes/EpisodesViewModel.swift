//
//  EpisodesViewModel.swift
//  BBApp
//
//  Created by James Bellamy on 29/05/2021.
//

import Combine
import Foundation

final class EpisodesViewModel: ObservableObject {
    // Service
    private var episodesService: BreakingBadEpisodeServiceProtocol
    
    // Requests
    @Published private(set) var isFetching: Bool = false
    private var cancellables: [AnyCancellable] = []
    
    // State
    var seriesFilter: String?
    
    // Alerts
    private let alertSubject: PassthroughSubject<AlertModel, Never> = .init()
    var alertPublisher: AnyPublisher<AlertModel, Never> {
        self.alertSubject.eraseToAnyPublisher()
    }
    
    // Model
    @Published private(set) var episodes: [BreakingBadEpisode] = []
    
    // MARK: Lifecycle
    
    init(episodesService: BreakingBadEpisodeServiceProtocol = BreakingBadEpisodesService()) {
        self.episodesService = episodesService
    }
    
    // MARK: Actions
    
    func reset() {
        // State
        self.seriesFilter = nil
        // Fetch
        self.fetch()
    }
    
    func fetch() {
        guard self.isFetching == false else { return }
        self.isFetching = true
        self.episodesService.getAllEpisodes(series: self.seriesFilter).sink(
            receiveCompletion: { [weak self] result in
                guard let self = self else { return }
                self.isFetching = false
                switch result {
                case .failure: self.alertSubject.send(AlertModel.genericError)
                case .finished: break
                }
            },
            receiveValue: { [weak self] (episodes: [BreakingBadEpisode]) in
                guard let self = self else { return }
                self.episodes = episodes
            }
        ).store(in: &self.cancellables)
    }
}
