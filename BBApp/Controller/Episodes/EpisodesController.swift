//
//  EpisodesController.swift
//  BBApp
//
//  Created by James Bellamy on 29/05/2021.
//

import Combine
import UIKit

private enum Sections: Int, CaseIterable {
    case episodes
}

private typealias DataSource = UICollectionViewDiffableDataSource<Sections, AnyHashable>
private typealias DataSourceSnapshot = NSDiffableDataSourceSnapshot<Sections, AnyHashable>

private let defaultCellHeight: CGFloat = 100.0

final class EpisodesController: UIViewController {
    
    @IBOutlet private var collectionContainerView: UIView!
    
    private let searchController = UISearchController(searchResultsController: nil)
    
    private var cancellables: [AnyCancellable] = []
    
    var viewModel: EpisodesViewModel = EpisodesViewModel()
    
    // MARK: Collection View
    
    private lazy var collectionView: UICollectionView = {
        let layout = self.collectionViewLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(cell: EpisodeSummaryCell.self)
        collectionView.delegate = self
        collectionView.alwaysBounceVertical = true
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refetch), for: .valueChanged)
        collectionView.refreshControl = refreshControl
        return collectionView
    }()
    
    private func collectionViewLayout() -> UICollectionViewLayout {
        let configuration = UICollectionViewCompositionalLayoutConfiguration()
        configuration.scrollDirection = .vertical
        return UICollectionViewCompositionalLayout(sectionProvider: { (sectionIndex, environment) -> NSCollectionLayoutSection? in
            let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(defaultCellHeight))
            let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(defaultCellHeight))
            let item = NSCollectionLayoutItem(layoutSize: itemSize)
            let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitems: [item])
            let section = NSCollectionLayoutSection(group: group)
            section.interGroupSpacing = 10
            section.contentInsets = NSDirectionalEdgeInsets(top: 20, leading: 20, bottom: 20, trailing: 10)
            return section
        }, configuration: configuration)
    }
    
    private lazy var collectionViewDataSource: DataSource = {
        DataSource(collectionView: self.collectionView) { (collectionView, indexPath, model) -> UICollectionViewCell? in
            switch Sections(rawValue: indexPath.section) {
            case .episodes:
                if let model = model as? BreakingBadEpisode, let cell = collectionView.dequeue(cell: EpisodeSummaryCell.self, for: indexPath) {
                    let cellViewModel = EpisodeSummaryCellViewModel(
                        title: model.title,
                        episode: model.episode,
                        airDate: model.airDate
                    )
                    cell.configure(cellViewModel)
                    return cell
                }
            case .none: break
            }
            return nil
        }
    }()
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Title
        self.title = "Episodes"
        // Layout
        self.collectionView.fixInView(self.collectionContainerView)
        // Setup
        self.setupAppearance()
        self.setupBindings()
        self.setupSearch()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Fetch
        if self.viewModel.episodes.isEmpty {
            self.viewModel.fetch()
        }
    }
    
    // MARK: Setup
    
    private func setupAppearance() {
        self.collectionView.backgroundColor = .clear
        self.collectionContainerView.backgroundColor = .clear
    }
    
    private func setupBindings() {
        self.viewModel.$isFetching.sink(
            receiveValue: { [weak self] (isFetching) in
                guard let self = self else { return }
                DispatchQueue.main.async {
                    if isFetching == false {
                        self.collectionView.refreshControl?.endRefreshing()
                    }
                }
            }
        ).store(in: &self.cancellables)
        self.viewModel.$episodes.sink(
            receiveValue: { [weak self] (episodes) in
                guard let self = self else { return }
                DispatchQueue.main.async {
                    self.updateDataSource(episodes)
                }
            }
        ).store(in: &self.cancellables)
        self.viewModel.alertPublisher.sink(
            receiveValue: { [weak self] (model) in
                guard let self = self else { return }
                DispatchQueue.main.async {
                    self.present(UIAlertController.alert(for: model), animated: true, completion: nil)
                }
            }
        ).store(in: &self.cancellables)
    }
    
    private func setupSearch() {
        self.searchController.obscuresBackgroundDuringPresentation = false
        self.searchController.searchBar.placeholder = "Search by Series"
        self.searchController.searchBar.delegate = self
        self.navigationItem.searchController = searchController
        self.definesPresentationContext = true
    }
    
    // MARK: Model
    
    private func updateDataSource(_ episodes: [BreakingBadEpisode] = []) {
        DispatchQueue.main.async {
            var snapshot = DataSourceSnapshot()
            snapshot.appendSections(Sections.allCases)
            snapshot.appendItems(episodes, toSection: .episodes)
            self.collectionViewDataSource.apply(snapshot, animatingDifferences: true)
        }
    }
    
    // MARK: Actions
    
    @objc private func refetch() {
        self.viewModel.reset()
    }
    
    // MARK: Navigation
    
    private func displayContentDetails(episode: BreakingBadEpisode) {
        guard let controller = ContentDetailsController.instantiateFromStoryboard() else { return }
        controller.viewModel = ContentDetailsViewModel(episode: episode)
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

// MARK: - StoryboardView

extension EpisodesController: StoryboardView {
    static var storyboard: Storyboard = .main
}

// MARK: - UICollectionViewDelegate

extension EpisodesController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let episode = self.viewModel.episodes[indexPath.row]
        self.displayContentDetails(episode: episode)
    }
}

// MARK: - UISearchBarDelegate

extension EpisodesController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.viewModel.seriesFilter = searchText.replacingOccurrences(of: " ", with: "+")
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.viewModel.seriesFilter = nil
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.viewModel.fetch()
        self.searchController.isActive = false
    }
}
