//
//  TabController.swift
//  BBApp
//
//  Created by James Bellamy on 29/05/2021.
//

import UIKit

final class TabController: UITabBarController, StoryboardView {
    static var storyboard: Storyboard = .main
}
