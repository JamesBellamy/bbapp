//
//  CharactersViewModel.swift
//  BBApp
//
//  Created by James Bellamy on 29/05/2021.
//

import Combine
import Foundation

enum CharacterFilter: String, CaseIterable {
    case name = "Name"
    case category = "Series"
}

final class CharactersViewModel: ObservableObject {
    // Const
    var limit: Int {
        self.charactersService.limit
    }
    
    // Service
    private var charactersService: BreakingBadCharactersServiceProtocol
    
    // Requests
    @Published private(set) var isFetching: Bool = false
    private var cancellables: [AnyCancellable] = []
    
    // State
    var filterType: CharacterFilter = .name
    var filterText: String?
    
    // Alerts
    private let alertSubject: PassthroughSubject<AlertModel, Never> = .init()
    var alertPublisher: AnyPublisher<AlertModel, Never> {
        self.alertSubject.eraseToAnyPublisher()
    }
    
    // Model
    @Published private(set) var characters: [BreakingBadCharacter] = []
    
    // MARK: Lifecycle
    
    init(charactersService: BreakingBadCharactersServiceProtocol = BreakingBadCharactersService(limit: 10)) {
        self.charactersService = charactersService
    }
    
    // MARK: Checks
    
    private func canFetch() -> Bool {
        self.charactersService.canFetch == true && self.isFetching == false
    }
    
    // MARK: Actions
    
    func reset(filter: Bool = true) {
        // State
        if filter == true {
            self.filterText = nil
            self.filterType = .name
        }
        // Requests
        self.cancellables = []
        self.isFetching = false
        // Service
        self.charactersService.reset()
        // Fetch
        self.fetch(isReset: true)
    }
    
    func fetch(isReset: Bool = false) {
        guard self.canFetch() else { return }
        self.isFetching = true
        let category: String? = self.filterType == .category ? self.filterText : nil
        let name: String? = self.filterType == .name ? self.filterText : nil
        self.charactersService.getAllCharacters(category: category, name: name).sink(
            receiveCompletion: { [weak self] result in
                guard let self = self else { return }
                self.isFetching = false
                switch result {
                case .failure: self.alertSubject.send(AlertModel.genericError)
                case .finished: break
                }
            },
            receiveValue: { [weak self] (characters: [BreakingBadCharacter]) in
                guard let self = self else { return }
                if isReset {
                    self.characters = characters
                } else {
                    self.characters.append(contentsOf: characters)
                }
                
            }
        ).store(in: &self.cancellables)
    }
}
