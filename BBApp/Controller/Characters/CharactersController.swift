//
//  CharactersController.swift
//  BBApp
//
//  Created by James Bellamy on 29/05/2021.
//

import Combine
import UIKit

private enum Sections: Int, CaseIterable {
    case characters
}

private typealias DataSource = UICollectionViewDiffableDataSource<Sections, AnyHashable>
private typealias DataSourceSnapshot = NSDiffableDataSourceSnapshot<Sections, AnyHashable>

private let defaultCellHeight: CGFloat = 225.0

final class CharactersController: UIViewController {
    
    @IBOutlet private var collectionContainerView: UIView!
    
    private let searchController = UISearchController(searchResultsController: nil)
    
    private var cancellables: [AnyCancellable] = []
    
    var viewModel: CharactersViewModel = CharactersViewModel()
    
    // MARK: Collection View
    
    private lazy var collectionView: UICollectionView = {
        let layout = self.collectionViewLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(cell: CharacterSummaryCell.self)
        collectionView.delegate = self
        collectionView.alwaysBounceVertical = true
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refetch), for: .valueChanged)
        collectionView.refreshControl = refreshControl
        return collectionView
    }()
    
    private func collectionViewLayout() -> UICollectionViewLayout {
        let configuration = UICollectionViewCompositionalLayoutConfiguration()
        configuration.scrollDirection = .vertical
        return UICollectionViewCompositionalLayout(sectionProvider: { [weak self] (sectionIndex, environment) -> NSCollectionLayoutSection? in
            guard let self = self else { return nil }
            let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(defaultCellHeight))
            let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(defaultCellHeight))
            let item = NSCollectionLayoutItem(layoutSize: itemSize)
            let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitems: [item])
            let section = NSCollectionLayoutSection(group: group)
            section.interGroupSpacing = 20
            section.contentInsets = NSDirectionalEdgeInsets(top: 20, leading: 20, bottom: 20, trailing: 10)
            section.visibleItemsInvalidationHandler = { [weak self] visibleItems, point, _ in
                guard let self = self else { return }
                let buffer: Int = Int(Double(self.viewModel.limit) * 0.5)
                let maxIndex: Int = self.viewModel.characters.count
                let maxVisible: Int = visibleItems.map { (item) -> IndexPath in item.indexPath }.max()?.row ?? 0
                if point.y > 0, maxVisible + buffer >= maxIndex, self.viewModel.isFetching == false {
                    self.viewModel.fetch()
                }
            }
            return section
        }, configuration: configuration)
    }
    
    private lazy var collectionViewDataSource: DataSource = {
        DataSource(collectionView: self.collectionView) { (collectionView, indexPath, model) -> UICollectionViewCell? in
            switch Sections(rawValue: indexPath.section) {
            case .characters:
                if let model = model as? BreakingBadCharacter, let cell = collectionView.dequeue(cell: CharacterSummaryCell.self, for: indexPath) {
                    let cellViewModel = CharacterSummaryCellViewModel(
                        imageUrl: model.imageUrl,
                        name: model.name,
                        nickname: model.nickname,
                        status: model.status
                    )
                    cell.configure(cellViewModel)
                    return cell
                }
            case .none: break
            }
            return nil
        }
    }()
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Title
        self.title = "Characters"
        // Layout
        self.collectionView.fixInView(self.collectionContainerView)
        // Setup
        self.setupAppearance()
        self.setupBindings()
        self.setupSearch()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Fetch
        if self.viewModel.characters.isEmpty {
            self.viewModel.fetch()
        }
    }
    
    // MARK: Setup
    
    private func setupAppearance() {
        self.collectionView.backgroundColor = .clear
        self.collectionContainerView.backgroundColor = .clear
    }
    
    private func setupBindings() {
        self.viewModel.$isFetching.sink(
            receiveValue: { [weak self] (isFetching) in
                guard let self = self else { return }
                DispatchQueue.main.async {
                    if isFetching == false {
                        self.collectionView.refreshControl?.endRefreshing()
                    }
                }
            }
        ).store(in: &self.cancellables)
        self.viewModel.$characters.sink(
            receiveValue: { [weak self] (characters) in
                guard let self = self else { return }
                DispatchQueue.main.async {
                    self.updateDataSource(characters)
                }
            }
        ).store(in: &self.cancellables)
        self.viewModel.alertPublisher.sink(
            receiveValue: { [weak self] (model) in
                guard let self = self else { return }
                DispatchQueue.main.async {
                    self.present(UIAlertController.alert(for: model), animated: true, completion: nil)
                }
            }
        ).store(in: &self.cancellables)
    }
    
    private func setupSearch() {
        self.searchController.obscuresBackgroundDuringPresentation = false
        self.searchController.searchBar.placeholder = "Search"
        self.searchController.searchBar.delegate = self
        self.searchController.searchBar.scopeButtonTitles = CharacterFilter.allCases.map({ (type) -> String in type.rawValue })
        self.navigationItem.searchController = searchController
        self.definesPresentationContext = true
    }
    
    // MARK: Model
    
    private func updateDataSource(_ characters: [BreakingBadCharacter] = []) {
        var snapshot = DataSourceSnapshot()
        snapshot.appendSections(Sections.allCases)
        snapshot.appendItems(characters, toSection: .characters)
        self.collectionViewDataSource.apply(snapshot, animatingDifferences: true)
    }
    
    // MARK: Actions
    
    @objc private func refetch() {
        self.viewModel.reset()
    }
    
    // MARK: Navigation
    
    private func displayContentDetails(character: BreakingBadCharacter) {
        guard let controller = ContentDetailsController.instantiateFromStoryboard() else { return }
        controller.viewModel = ContentDetailsViewModel(character: character)
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

// MARK: - StoryboardView

extension CharactersController: StoryboardView {
    static var storyboard: Storyboard = .main
}

// MARK: - UICollectionViewDelegate

extension CharactersController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let character = self.viewModel.characters[indexPath.row]
        self.displayContentDetails(character: character)
    }
}

// MARK: - UISearchBarDelegate

extension CharactersController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.viewModel.filterType = CharacterFilter(rawValue: searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]) ?? .name
        self.viewModel.filterText = searchText.replacingOccurrences(of: " ", with: "+")
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.viewModel.filterType = .name
        self.viewModel.filterText = nil
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.viewModel.reset(filter: false)
        self.searchController.isActive = false
    }
}
