//
//  ContentDetailsViewModel.swift
//  BBApp
//
//  Created by James Bellamy on 30/05/2021.
//

import Combine
import Foundation

typealias ContentDetail = (title: String, detail: String?)

struct ContentDetailsViewModel {
    var title: String?
    var details: [ContentDetail] = []
    
    init(character: BreakingBadCharacter) {
        self.title = "Character Details"
        self.details = [
            ("Name", character.name),
            ("Nickname", character.nickname),
            ("Birthday", character.birthday),
            ("Occupations", character.occupation?.joined(separator: ", ")),
            ("Status", character.status),
            ("Seasons Featured", character.appearance?
                .map({ (season) -> String in String(describing: season) })
                .joined(separator: ", ") ?? "-"
            ),
            ("Portrayed by", character.portrayed)
        ]
    }
    
    init(episode: BreakingBadEpisode) {
        self.title = "Character Details"
        self.details = [
            ("Title", episode.title),
            ("Episode", episode.episode),
            ("Air Date", episode.airDate),
            ("Series", episode.series),
            ("Season", episode.season),
            ("Characters", episode.characters?.joined(separator: ", "))
        ]
    }
    
    init(quote: BreakingBadQuote) {
        self.title = "Character Details"
        self.details = [
            ("Quote", quote.quote),
            ("Author", quote.author)
        ]
    }
}
