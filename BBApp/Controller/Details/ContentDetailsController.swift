//
//  ContentDetailsController.swift
//  BBApp
//
//  Created by James Bellamy on 30/05/2021.
//

import Combine
import UIKit

private let defaultCellHeight: CGFloat = 50.0

final class ContentDetailsController: UIViewController {
    
    @IBOutlet private var collectionContainerView: UIView!
    
    private var cancellables: [AnyCancellable] = []
    
    var viewModel: ContentDetailsViewModel!
    
    // MARK: Container View
    
    private lazy var collectionView: UICollectionView = {
        let layout = self.collectionViewLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(cell: ContentDetailsCell.self)
        collectionView.dataSource = self
        collectionView.alwaysBounceVertical = true
        return collectionView
    }()
    
    private func collectionViewLayout() -> UICollectionViewLayout {
        let configuration = UICollectionViewCompositionalLayoutConfiguration()
        configuration.scrollDirection = .vertical
        return UICollectionViewCompositionalLayout(sectionProvider: { (sectionIndex, environment) -> NSCollectionLayoutSection? in
            let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(defaultCellHeight))
            let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(defaultCellHeight))
            let item = NSCollectionLayoutItem(layoutSize: itemSize)
            let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitems: [item])
            let section = NSCollectionLayoutSection(group: group)
            section.interGroupSpacing = 20
            section.contentInsets = NSDirectionalEdgeInsets(top: 20, leading: 20, bottom: 20, trailing: 10)
            return section
        }, configuration: configuration)
    }
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Title
        self.title = self.viewModel.title ?? "Details"
        // Layout
        self.collectionView.fixInView(self.collectionContainerView)
        // Setup
        self.setupAppearance()
    }
    
    // MARK: Setup
    
    func setupAppearance() {
        self.collectionView.backgroundColor = .clear
        self.collectionContainerView.backgroundColor = .clear
    }
}

// MARK: - StoryboardView

extension ContentDetailsController: StoryboardView {
    static var storyboard: Storyboard = .details
}

// MARK: - UICollectionViewDataSource

extension ContentDetailsController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.viewModel.details.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeue(cell: ContentDetailsCell.self, for: indexPath) else {
            return UICollectionViewCell()
        }
        let content = self.viewModel.details[indexPath.row]
        let cellViewModel = ContentDetailsCellViewModel(title: content.title, details: content.detail)
        cell.configure(cellViewModel)
        return cell
    }
}
