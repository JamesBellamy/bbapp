//
//  QuotesViewModel.swift
//  BBApp
//
//  Created by James Bellamy on 29/05/2021.
//

import Combine
import Foundation

final class QuotesViewModel {
    // Service
    private var quotesService: BreakingBadQuotesServiceProtocol
    
    // Requests
    @Published private(set) var isFetching: Bool = false
    private var cancellables: [AnyCancellable] = []
    
    // Alerts
    private let alertSubject: PassthroughSubject<AlertModel, Never> = .init()
    var alertPublisher: AnyPublisher<AlertModel, Never> {
        self.alertSubject.eraseToAnyPublisher()
    }
    
    // Model
    @Published private(set) var quotes: [BreakingBadQuote] = []
    
    // MARK: Lifecycle
    
    init(quotesService: BreakingBadQuotesServiceProtocol = BreakingBadQuotesService()) {
        self.quotesService = quotesService
    }
    
    // MARK: Actions
    
    func fetch() {
        guard self.isFetching == false else { return }
        self.isFetching = true
        self.quotesService.getAllQuotes().sink(
            receiveCompletion: { [weak self] result in
                guard let self = self else { return }
                self.isFetching = false
                switch result {
                case .failure: self.alertSubject.send(AlertModel.genericError)
                case .finished: break
                }
            },
            receiveValue: { [weak self] (quotes: [BreakingBadQuote]) in
                guard let self = self else { return }
                self.quotes = quotes
            }
        ).store(in: &self.cancellables)
    }
}
