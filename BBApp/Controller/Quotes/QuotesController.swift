//
//  QuotesController.swift
//  BBApp
//
//  Created by James Bellamy on 29/05/2021.
//

import Combine
import UIKit

private enum Sections: Int, CaseIterable {
    case quotes
}

private typealias DataSource = UICollectionViewDiffableDataSource<Sections, AnyHashable>
private typealias DataSourceSnapshot = NSDiffableDataSourceSnapshot<Sections, AnyHashable>

private let defaultCellHeight: CGFloat = 50.0

final class QuotesController: UIViewController {
    
    @IBOutlet private var collectionContainerView: UIView!
    
    private var cancellables: [AnyCancellable] = []
    
    var viewModel: QuotesViewModel = QuotesViewModel()
    
    // MARK: Collection View
    
    private lazy var collectionView: UICollectionView = {
        let layout = self.collectionViewLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(cell: QuotesSummaryCell.self)
        collectionView.delegate = self
        collectionView.alwaysBounceVertical = true
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refetch), for: .valueChanged)
        collectionView.refreshControl = refreshControl
        return collectionView
    }()
    
    private func collectionViewLayout() -> UICollectionViewLayout {
        let configuration = UICollectionViewCompositionalLayoutConfiguration()
        configuration.scrollDirection = .vertical
        return UICollectionViewCompositionalLayout(sectionProvider: { (sectionIndex, environment) -> NSCollectionLayoutSection? in
            let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(defaultCellHeight))
            let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(defaultCellHeight))
            let item = NSCollectionLayoutItem(layoutSize: itemSize)
            let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitems: [item])
            let section = NSCollectionLayoutSection(group: group)
            section.interGroupSpacing = 20
            section.contentInsets = NSDirectionalEdgeInsets(top: 20, leading: 20, bottom: 20, trailing: 10)
            return section
        }, configuration: configuration)
    }
    
    private lazy var collectionViewDataSource: DataSource = {
        DataSource(collectionView: self.collectionView) { (collectionView, indexPath, model) -> UICollectionViewCell? in
            switch Sections(rawValue: indexPath.section) {
            case .quotes:
                if let model = model as? BreakingBadQuote, let cell = collectionView.dequeue(cell: QuotesSummaryCell.self, for: indexPath) {
                    let cellViewModel = QuotesSummaryCellViewModel(quote: model.quote, author: model.author)
                    cell.configure(cellViewModel)
                    return cell
                }
            case .none: break
            }
            return nil
        }
    }()
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Title
        self.title = "Quotes"
        // Layout
        self.collectionView.fixInView(self.collectionContainerView)
        // Setup
        self.setupAppearance()
        self.setupBindings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Fetch
        if self.viewModel.quotes.isEmpty {
            self.viewModel.fetch()
        }
    }
    
    // MARK: Setup
    
    private func setupAppearance() {
        self.collectionView.backgroundColor = .clear
        self.collectionContainerView.backgroundColor = .clear
    }
    
    private func setupBindings() {
        self.viewModel.$isFetching.sink(
            receiveValue: { [weak self] (isFetching) in
                guard let self = self else { return }
                DispatchQueue.main.async {
                    if isFetching == false {
                        self.collectionView.refreshControl?.endRefreshing()
                    }
                }
            }
        ).store(in: &self.cancellables)
        self.viewModel.$quotes.sink(
            receiveValue: { [weak self] (quotes) in
                guard let self = self else { return }
                DispatchQueue.main.async {
                    self.updateDataSource(quotes)
                }
            }
        ).store(in: &self.cancellables)
        self.viewModel.alertPublisher.sink(
            receiveValue: { [weak self] (model) in
                guard let self = self else { return }
                DispatchQueue.main.async {
                    self.present(UIAlertController.alert(for: model), animated: true, completion: nil)
                }
            }
        ).store(in: &self.cancellables)
    }
    
    // MARK: Model
    
    private func updateDataSource(_ quotes: [BreakingBadQuote] = []) {
        DispatchQueue.main.async {
            var snapshot = DataSourceSnapshot()
            snapshot.appendSections(Sections.allCases)
            snapshot.appendItems(quotes, toSection: .quotes)
            self.collectionViewDataSource.apply(snapshot, animatingDifferences: true)
        }
    }
    
    // MARK: Actions
    
    @objc private func refetch() {
        self.viewModel.fetch()
    }
    
    // MARK: Navigation
    
    private func displayContentDetails(quote: BreakingBadQuote) {
        guard let controller = ContentDetailsController.instantiateFromStoryboard() else { return }
        controller.viewModel = ContentDetailsViewModel(quote: quote)
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

// MARK: - StoryboardView

extension QuotesController: StoryboardView {
    static var storyboard: Storyboard = .main
}

// MARK: - UICollectionViewDelegate

extension QuotesController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let quote = self.viewModel.quotes[indexPath.row]
        self.displayContentDetails(quote: quote)
    }
}
