//
//  CharacterSummaryCellViewModel.swift
//  BBApp
//
//  Created by James Bellamy on 29/05/2021.
//

import Foundation

struct CharacterSummaryCellViewModel {
    var imageUrl: String?
    var name: String?
    var nickname: String?
    var status: String?
    
    init(imageUrl: String?, name: String?, nickname: String?, status: String?) {
        self.imageUrl = imageUrl
        self.name = name
        if let nickname = nickname, nickname.isEmpty == false {
            self.nickname = "AKA \(nickname)"
        } else {
            self.nickname = "Unknown Alias"
        }
        self.status = status
    }
}
