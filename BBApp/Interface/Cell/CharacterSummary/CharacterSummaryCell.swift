//
//  CharacterSummaryCell.swift
//  BBApp
//
//  Created by James Bellamy on 29/05/2021.
//

import UIKit

import Nuke

final class CharacterSummaryCell: UICollectionViewCell {
    @IBOutlet private var stackView: UIStackView!
    @IBOutlet private var detailsStackView: UIStackView!
    @IBOutlet private var labelsStackView: UIStackView!
    @IBOutlet private var imageView: UIImageView!
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var nicknameLabel: UILabel!
    @IBOutlet private var statusLabel: UILabel!
    @IBOutlet private var disclosureIconImageView: UIImageView!
    @IBOutlet private var separatorView: UIView!
    
    private var imageCancellable: ImageTask?
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.reset()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.reset()
    }
    
    func reset() {
        self.setupAppearance()
        self.setupState()
    }
    
    // MARK: Setup
    
    func setupAppearance() {
        // Separator
        self.separatorView.backgroundColor = UIColor(named: "Separator")
        // Stack
        self.stackView.spacing = 20
        // Name
        self.nameLabel.font = UIFont.boldSystemFont(ofSize: 20)
        self.nameLabel.textAlignment = .left
        self.nameLabel.numberOfLines = 0
        // Nickname
        self.nicknameLabel.font = UIFont.systemFont(ofSize: 16)
        self.nicknameLabel.textAlignment = .left
        self.nicknameLabel.numberOfLines = 0
        // Status
        self.statusLabel.font = UIFont.systemFont(ofSize: 16)
        self.statusLabel.textAlignment = .left
        self.statusLabel.numberOfLines = 0
    }
    
    func setupState() {
        self.imageCancellable?.cancel()
        self.imageCancellable = nil
        self.imageView.image = nil
        self.nameLabel.text = nil
        self.nicknameLabel.text = nil
        self.statusLabel.text = nil
    }
    
    // MARK: Configure
    
    func configure(_ viewModel: CharacterSummaryCellViewModel) {
        self.imageCancellable = self.imageView.fetchImage(at: viewModel.imageUrl)
        self.nameLabel.text = viewModel.name
        self.nicknameLabel.text = viewModel.nickname
        self.statusLabel.text = viewModel.status
    }
}
