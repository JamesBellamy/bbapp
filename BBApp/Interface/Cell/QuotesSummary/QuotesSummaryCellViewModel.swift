//
//  QuotesSummaryCellViewModel.swift
//  BBApp
//
//  Created by James Bellamy on 30/05/2021.
//

import Foundation

struct QuotesSummaryCellViewModel {
    var quote: String?
    var author: String?
}
