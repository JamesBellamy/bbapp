//
//  EpisodeSummaryCell.swift
//  BBApp
//
//  Created by James Bellamy on 30/05/2021.
//

import UIKit

final class EpisodeSummaryCell: UICollectionViewCell {
    @IBOutlet private var stackView: UIStackView!
    @IBOutlet private var labelStackView: UIStackView!
    @IBOutlet private var topLabelsStackView: UIStackView!
    @IBOutlet private var episodeNameLabel: UILabel!
    @IBOutlet private var episodeNumberLabel: UILabel!
    @IBOutlet private var airDateLabel: UILabel!
    @IBOutlet private var disclosureIconImageView: UIImageView!
    @IBOutlet private var separatorView: UIView!
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.reset()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.reset()
    }
    
    func reset() {
        self.setupAppearance()
        self.setupState()
    }
    
    // MARK: Setup
    
    func setupAppearance() {
        // Separator
        self.separatorView.backgroundColor = UIColor(named: "Separator")
        // Stack
        self.stackView.spacing = 20
        self.labelStackView.spacing = 10
        // Name
        self.episodeNameLabel.font = UIFont.boldSystemFont(ofSize: 20)
        self.episodeNameLabel.textAlignment = .justified
        self.episodeNameLabel.numberOfLines = 0
        // Number
        self.episodeNumberLabel.font = UIFont.systemFont(ofSize: 16)
        self.episodeNumberLabel.textAlignment = .justified
        self.episodeNumberLabel.numberOfLines = 1
        // Date
        self.airDateLabel.font = UIFont.systemFont(ofSize: 16)
        self.airDateLabel.textAlignment = .justified
        self.airDateLabel.numberOfLines = 1
    }
    
    func setupState() {
        self.episodeNameLabel.text = nil
        self.episodeNumberLabel.text = nil
        self.airDateLabel.text = nil
    }
    
    // MARK: Configure
    
    func configure(_ viewModel: EpisodeSummaryCellViewModel) {
        self.episodeNameLabel.text = viewModel.title
        self.episodeNumberLabel.text = viewModel.episode
        self.airDateLabel.text = viewModel.airDate
    }
}
