//
//  EpisodeSummaryCellViewModel.swift
//  BBApp
//
//  Created by James Bellamy on 30/05/2021.
//

import Foundation

struct EpisodeSummaryCellViewModel {
    var title: String?
    var episode: String?
    var airDate: String?
    
    init(title: String?, episode: String?, airDate: String?) {
        self.title = title
        if let episode = episode, episode.isEmpty == false {
            self.episode = "Episode #\(episode)"
        } else {
            self.episode = "Unknown Episode"
        }
        self.airDate = airDate
    }
}
