//
//  ContentDetailsCell.swift
//  BBApp
//
//  Created by James Bellamy on 30/05/2021.
//

import UIKit

final class ContentDetailsCell: UICollectionViewCell {
    @IBOutlet private var stackView: UIStackView!
    @IBOutlet private var labelStackView: UIStackView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var detailLabel: UILabel!
    @IBOutlet private var separatorView: UIView!
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.reset()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.reset()
    }
    
    func reset() {
        self.setupAppearance()
        self.setupState()
    }
    
    // MARK: Setup
    
    func setupAppearance() {
        // Separator
        self.separatorView.backgroundColor = UIColor(named: "Separator")
        // Stack
        self.stackView.spacing = 20
        self.labelStackView.spacing = 10
        // Quote
        self.titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
        self.titleLabel.textAlignment = .left
        self.titleLabel.numberOfLines = 0
        // Author
        self.detailLabel.font = UIFont.systemFont(ofSize: 14)
        self.detailLabel.textAlignment = .left
        self.detailLabel.numberOfLines = 0
    }
    
    func setupState() {
        self.titleLabel.text = nil
        self.detailLabel.text = nil
    }
    
    // MARK: Configure
    
    func configure(_ viewModel: ContentDetailsCellViewModel) {
        self.titleLabel.text = viewModel.title
        self.detailLabel.text = viewModel.details ?? "Unknown"
    }
}
