//
//  ContentDetailsCellViewModel.swift
//  BBApp
//
//  Created by James Bellamy on 30/05/2021.
//

import Foundation

struct ContentDetailsCellViewModel {
    var title: String
    var details: String?
}
