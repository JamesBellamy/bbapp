//
//  Storyboards.swift
//  BBApp
//
//  Created by James Bellamy on 30/05/2021.
//

import Foundation

enum Storyboard: String {
    case main = "Main"
    case details = "Details"
}
