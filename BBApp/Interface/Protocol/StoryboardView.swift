//
//  StoryboardView.swift
//  BBApp
//
//  Created by James Bellamy on 29/05/2021.
//

import UIKit

protocol StoryboardView: UIViewController {
    static var storyboard: Storyboard { get }
    static var storyboardId: String { get }
    
    static func instantiateFromStoryboard() -> Self?
}

extension StoryboardView {
    static var storyboardId: String { String(describing: self) }
    
    static func instantiateFromStoryboard() -> Self? {
        UIStoryboard(name: self.storyboard.rawValue, bundle: .main)
            .instantiateViewController(identifier: self.storyboardId)
    }
}
