//
//  QuotesSummary.swift
//  BBApp
//
//  Created by James Bellamy on 30/05/2021.
//

import UIKit

final class QuotesSummaryCell: UICollectionViewCell {
    @IBOutlet private var stackView: UIStackView!
    @IBOutlet private var labelStackView: UIStackView!
    @IBOutlet private var quoteLabel: UILabel!
    @IBOutlet private var authorLabel: UILabel!
    @IBOutlet private var disclosureIconImageView: UIImageView!
    @IBOutlet private var separatorView: UIView!
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.reset()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.reset()
    }
    
    func reset() {
        self.setupAppearance()
        self.setupState()
    }
    
    // MARK: Setup
    
    func setupAppearance() {
        // Separator
        self.separatorView.backgroundColor = UIColor(named: "Separator")
        // Stack
        self.stackView.spacing = 20
        self.labelStackView.spacing = 10
        // Quote
        self.quoteLabel.font = UIFont.systemFont(ofSize: 16)
        self.quoteLabel.textAlignment = .left
        self.quoteLabel.numberOfLines = 0
        // Author
        self.authorLabel.font = UIFont.italicSystemFont(ofSize: 14)
        self.authorLabel.textAlignment = .left
        self.authorLabel.numberOfLines = 1
    }
    
    func setupState() {
        self.quoteLabel.text = ""
        self.authorLabel.text = ""
    }
    
    // MARK: Configure
    
    func configure(_ viewModel: QuotesSummaryCellViewModel) {
        self.quoteLabel.text = viewModel.quote
        self.authorLabel.text = viewModel.author ?? "Unknown"
    }
}
