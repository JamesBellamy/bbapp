//
//  UICollectionView+Extensions.swift
//  BBApp
//
//  Created by James Bellamy on 29/05/2021.
//

import UIKit

extension UICollectionView {
    
    // MARK: Register
    
    func register(cell: UICollectionViewCell.Type) {
        let nib = UINib(nibName: cell.identifier, bundle: .main)
        self.register(nib, forCellWithReuseIdentifier: cell.identifier)
    }
    
    func register(header: UICollectionReusableView.Type) {
        let nib = UINib(nibName: header.identifier, bundle: .main)
        self.register(nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: header.identifier)
    }

    func register(footer: UICollectionReusableView.Type) {
        let nib = UINib(nibName: footer.identifier, bundle: .main)
        self.register(nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: footer.identifier)
    }
    
    // MARK: Dequeue

    func dequeue<T: UICollectionViewCell>(cell: T.Type, for indexPath: IndexPath) -> T? {
        self.dequeueReusableCell(withReuseIdentifier: cell.identifier, for: indexPath) as? T
    }

    func dequeue<T: UICollectionReusableView>(reusableView: T.Type, kind: String, for indexPath: IndexPath) -> T? {
        self.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: reusableView.identifier, for: indexPath) as? T
    }
}
