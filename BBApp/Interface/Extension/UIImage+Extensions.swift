//
//  UIImage+Extensions.swift
//  BBApp
//
//  Created by James Bellamy on 29/05/2021.
//

import UIKit

import func AVFoundation.AVMakeRect

import Nuke

private let defaultPlaceholderImage: UIImage = UIImage(systemName: "person.crop.circle")!

typealias FetchImageCompletion = (Result<ImageResponse, ImagePipeline.Error>) -> Void

extension UIImageView {
    func fetchImage(at imageUrl: String?, withPlaceholder placeholder: UIImage? = nil, completion: FetchImageCompletion? = nil) -> ImageTask? {
        var cancellable: ImageTask?
        let defaultImage: UIImage = placeholder ?? defaultPlaceholderImage
        if let imageUrl = imageUrl, imageUrl.isEmpty == false, let url = URL(string: imageUrl) {
            let options = ImageLoadingOptions(transition: .fadeIn(duration: 0.33), failureImage: defaultImage)
            let request = ImageRequest(url: url, processors: [])
            cancellable = Nuke.loadImage(with: request, options: options, into: self, completion: completion)
        } else {
            self.image = defaultImage
        }
        return cancellable
    }
}
