//
//  UIAlertController+Extensions.swift
//  BBApp
//
//  Created by James Bellamy on 30/05/2021.
//

import UIKit

extension UIAlertController {
    static func alert(for model: AlertModel, customActions: Bool = false) -> UIAlertController {
        let controller = UIAlertController(title: model.title, message: model.description, preferredStyle: .alert)
        if customActions == false {
            controller.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { [weak controller] _ in
                controller?.dismiss(animated: true, completion: nil)
            }))
        }
        return controller
    }
}
