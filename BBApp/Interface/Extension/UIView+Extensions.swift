//
//  UIView+Identifiable.swift
//  BBApp
//
//  Created by James Bellamy on 29/05/2021.
//

import UIKit

extension UIView {
    
    // MARK: Identifiable
    
    static var identifier: String {
        String(describing: self)
    }
    
    // MARK: Constraints
    
    func fixInView(_ container: UIView!) {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.frame = container.frame
        container.addSubview(self)
        NSLayoutConstraint.activate([
            self.leadingAnchor.constraint(equalTo: container.safeAreaLayoutGuide.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: container.safeAreaLayoutGuide.trailingAnchor),
            self.topAnchor.constraint(equalTo: container.safeAreaLayoutGuide.topAnchor),
            self.bottomAnchor.constraint(equalTo: container.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
}
