//
//  Alert.swift
//  BBApp
//
//  Created by James Bellamy on 29/05/2021.
//

import Combine
import Foundation

struct AlertModel {
    let title: String
    let description: String?
}

extension AlertModel {
    static let genericError = AlertModel(
        title: "Oops, something went wrong",
        description: "Please try again later"
    )
}
