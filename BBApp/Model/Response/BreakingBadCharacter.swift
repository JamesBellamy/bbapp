//
//  Character.swift
//  BBApp
//
//  Created by James Bellamy on 29/05/2021.
//

import Foundation

struct BreakingBadCharacter: Codable, Hashable {
    let charId: Int?
    let name: String?
    let birthday: String?
    let occupation: [String]?
    let imageUrl: String?
    let status: String?
    let appearance: [Int]?
    let nickname: String?
    let portrayed: String?

    enum CodingKeys: String, CodingKey {
        case charId = "char_id"
        case name = "name"
        case birthday = "birthday"
        case occupation = "occupation"
        case imageUrl = "img"
        case status = "status"
        case appearance = "appearance"
        case nickname = "nickname"
        case portrayed = "portrayed"
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.charId)
    }

    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.charId == rhs.charId
    }
}
