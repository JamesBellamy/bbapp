//
//  Quote.swift
//  BBApp
//
//  Created by James Bellamy on 29/05/2021.
//

import Foundation

struct BreakingBadQuote: Codable, Hashable {
    let quoteId: Int?
    let quote: String?
    let author: String?

    enum CodingKeys: String, CodingKey {
        case quoteId = "quote_id"
        case quote = "quote"
        case author = "author"
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.quoteId)
    }

    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.quoteId == rhs.quoteId
    }
}
