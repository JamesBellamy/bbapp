//
//  Episode.swift
//  BBApp
//
//  Created by James Bellamy on 29/05/2021.
//

import Foundation

struct BreakingBadEpisode: Codable, Hashable {
    let episodeId: Int?
    let title: String?
    let season: String?
    let episode: String?
    let airDate: String?
    let characters: [String]?
    let series: String?

    enum CodingKeys: String, CodingKey {
        case episodeId = "episode_id"
        case title = "title"
        case season = "season"
        case episode = "episode"
        case airDate = "air_date"
        case characters = "characters"
        case series = "series"
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.episodeId)
    }

    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.episodeId == rhs.episodeId
    }
}
