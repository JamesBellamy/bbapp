//
//  BreakingBadAPIEndpoints.swift
//  BBApp
//
//  Created by James Bellamy on 29/05/2021.
//

import Foundation

enum BreakingBadEndpoint {
    // Characters
    case getCharacters(limit: Int, offset: Int, category: String?, name: String?)
    case getCharacterById(id: String)
    case getRandomCharacter
    // Episodes
    case getEpisodes(series: String?)
    case getEpisodeById(id: String)
    // Quotes
    case getQuotes
    case getQuotesFiltered(series: String?, author: String?)
    case getQuoteById(id: String)
    case getRandomQuote(author: String?)
}

// MARK: - APIRequest

extension BreakingBadEndpoint: APIRequest {
    
    var host: String { "www.breakingbadapi.com" }
    
    private var pathPrefix: String { "api" }
    
    var path: String {
        var path: String = ""
        switch self {
        case .getCharacters:
            path = "characters"
        case let .getCharacterById(id):
            path = "characters/\(id)"
        case .getRandomCharacter:
            path = "character/random"
        case .getEpisodes:
            path = "episodes"
        case let .getEpisodeById(id):
            path = "episodes/\(id)"
        case .getQuotes:
            path = "quotes"
        case .getQuotesFiltered:
            path = "quote"
        case let .getQuoteById(id):
            path = "quotes/\(id)"
        case .getRandomQuote:
            path = "quote/random"
        }
        return "/\(self.pathPrefix)/\(path)"
    }
    
    var method: APIHTTPMethod {
        switch self {
        default:
            return .get
        }
    }
    
    var headers: [String : String] {
        return [:]
    }
    
    var queryItems: [URLQueryItem] {
        var items: [URLQueryItem] = []
        switch self {
        case let .getCharacters(limit, offset, category, name):
            items.append(self.integerQueryItem(name: "limit", value: limit))
            items.append(self.integerQueryItem(name: "offset", value: offset))
            if let item = self.optionalStringQueryItem(name: "category", value: category) {
                items.append(item)
            }
            if let item = self.optionalStringQueryItem(name: "name", value: name) {
                items.append(item)
            }
        case let .getEpisodes(series):
            if let item = self.optionalStringQueryItem(name: "series", value: series) {
                items.append(item)
            }
        case let .getQuotesFiltered(series, author):
            if let item = self.optionalStringQueryItem(name: "series", value: series) {
                items.append(item)
            }
            if let item = self.optionalStringQueryItem(name: "author", value: author) {
                items.append(item)
            }
        case let .getRandomQuote(author):
            if let item = self.optionalStringQueryItem(name: "author", value: author) {
                items.append(item)
            }
        default:
            break
        }
        return items
    }
    
    var body: Data? {
        return nil
    }
}
