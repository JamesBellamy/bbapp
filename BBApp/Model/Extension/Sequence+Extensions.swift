//
//  Sequence+Extensions.swift
//  BBApp
//
//  Created by James Bellamy on 30/05/2021.
//

import Foundation

extension Sequence where Iterator.Element: Hashable {
    func unique() -> [Iterator.Element] {
        var seen: Set<Iterator.Element> = []
        return filter { (element) -> Bool in seen.insert(element).inserted }
    }
}
