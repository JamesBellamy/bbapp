# BBApp

## Setup

- Run: `bundle install` to set up cocoapods
- Run: `pod install` to install dependencies

---

## Structure

This project follows an MVVM architecture considering SOLID principles and testability in layer separation. The app is split across three tabs for each of the data types exposed by Breaking Bad API. Features search functionality for Episodes and Characters, pagination for Characters, and a detail view for each data type. The app relies solely on iOS design elements (accent colour) for style (works in both dark and light mode) and primarily uses collection views.

### API Layer

This layer is project independent and could me exposed as a framework, and migrated to other apps.

Consists primarily of an API Client class which is set up to take any protocol-conforming request and return a Combine Publisher of the request.

### Model Layer

This layer is Breaking Bad API specific as it is the designated data model of this project, although further APIs and Models could be added that conform to the same protocols.

Consists primarily of an Endpoints enum and Response models. The Endpoints enum describes all possible api requests to Breaking Bad API, and the components required to construct a request. The Response models describe the data entities described in the API definition.

### Service Layer

This layer brings together the Model and API layers, and can provide logic for requests made if appropriate (offset pagination logic).

Consists primarily of base service definitions and domain specific services that return request publishers.

### View Layer

This layer is app specific Views and ViewModels. ViewModels are set up for both view controllers and collection cells.

A UIKit code-configured storyboard and cell xib approach was taken. This allows for visual construction of views that are configured and adjustable at the code level. Extensions were added to provide extra functionality or syntactic sugar to UIKit.

---

## TODO

### Meta

- Linting (SwiftLint) and Formatting (SwiftFormat)
- Type safe asset references (SwiftGen)
- Develop unit tests for each layer

### UI

- Detail page specific to each data model
- Detail pages with context appropriate content (Characters in an episode as a horizontal collection view / Quotes from a character on their detail view)

### Architecture

- Pagination logic could be moved to service dependency rather than superclass
- Coordinator / Navigator pattern to move navigation code outside of controllers to allow flexibility with displaying further views
- Consider if usage of Combine between layers is optimal
- Better definition of protocols to achieve Dependency Inversion

---

## Credit

- Color Scheme: https://www.schemecolor.com/breaking-bad-color-palette.php
- App Icon (Edited): https://hdclipartall.com/img-7791.html
- Nuke: https://github.com/kean/Nuke
