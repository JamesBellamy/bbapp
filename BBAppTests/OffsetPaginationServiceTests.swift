//
//  OffsetPaginationServiceTests.swift
//  BBAppTests
//
//  Created by James Bellamy on 30/05/2021.
//

import Foundation

import XCTest
@testable import BBApp

class OffsetPaginationServiceTests: XCTestCase {

    func testDefaultState() {
        // Given
        let service = OffsetPaginationService(limit: 10)
        
        // Then
        XCTAssertEqual(service.canFetch, true)
        XCTAssertEqual(service.limit, 10)
        XCTAssertEqual(service.offset, 0)
    }
    
    func testNextActionFullPage() {
        // Given
        let service = OffsetPaginationService(limit: 10)
        XCTAssertEqual(service.canFetch, true)
        XCTAssertEqual(service.offset, 0)
        
        // When
        service.next(after: 10)
        
        // Then
        XCTAssertEqual(service.canFetch, true)
        XCTAssertEqual(service.offset, 10)
    }
    
    func testNextActionPartialPage() {
        // Given
        let service = OffsetPaginationService(limit: 10)
        XCTAssertEqual(service.canFetch, true)
        XCTAssertEqual(service.offset, 0)
        
        // When
        service.next(after: 5)
        
        // Then
        XCTAssertEqual(service.canFetch, false)
        XCTAssertEqual(service.offset, 0)
    }
    
    func testResetAction() {
        // Given
        let service = OffsetPaginationService(limit: 10)
        service.next(after: 10)
        service.next(after: 5)
        XCTAssertEqual(service.canFetch, false)
        XCTAssertEqual(service.offset, 10)
        
        // When
        service.reset()
        
        // Then
        XCTAssertEqual(service.canFetch, true)
        XCTAssertEqual(service.offset, 0)
    }
}
